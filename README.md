# xml2word

#### 介绍
使用freemarker生成word文档

项目中有两个类：一个`XmlToDoc.class`直接执行main方法，可以生成一个doc格式的word文档，但是该文档其实是xml格式，只是将xml后缀直接改为doc，所以生成的文档在手机wps中打开会显示成xml；一个`XmlToDocx.class`直接执行main方法后，会生成一个docx格式的word文档，由于docx其实就是一个zip文件，文档内容为zip包下面的word/document.xml文件，所以这里直接修改的document.xml文件就可以改变内容，生成的docx文件可以在手机中正常打开。

执行main方法生成的word文档在target/classes/out目录下。

