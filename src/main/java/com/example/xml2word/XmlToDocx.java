package com.example.xml2word;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.Version;

import java.io.*;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * @author jiyupeng
 * 生成docx格式的word文档
 * word2007的docx格式的文档，其实就是zip文件，可以直接使用解压缩工具解压
 * 会在里面看到一个word目录，在word目录中有个document.xml文件，就是word文档内容
 */
public class XmlToDocx {
    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void main(String[] args) throws IOException, TemplateException {
        //使用模板生成document.xml文件
        Configuration configuration = new Configuration(Configuration.getVersion());
        configuration.setDefaultEncoding("UTF-8");

        URL resource = XmlToDoc.class.getClassLoader().getResource("");
        if(resource != null){
            File resourceDir = new File(resource.getFile());
            configuration.setDirectoryForTemplateLoading(resourceDir);
            Template template = configuration.getTemplate("docxTemplate.ftl");

            File tempFile = new File(resource.getFile() + "temp/docxTemp.xml");
            File tempDir = tempFile.getParentFile();
            if(!tempDir.exists()) {
                tempDir.mkdirs();
            }
            FileOutputStream tempFileOut = new FileOutputStream(tempFile);
            BufferedWriter tempWriter = new BufferedWriter(new OutputStreamWriter(tempFileOut));
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("title", "标题");
            dataMap.put("persons", DataGenerate.getData());
            template.process(dataMap, tempWriter);
            tempWriter.flush();
            tempWriter.close();

            //使用生成的xml文件来替换docx中的document.xml
            ZipFile docxTempFile = new ZipFile(resource.getFile() + "template.docx");
            File docxFile = new File(resource.getFile() + "out/导出.docx");
            File docxDir = docxFile.getParentFile();
            if(!docxDir.exists()){
                docxDir.mkdirs();
            }
            if(!docxFile.exists()) {
                docxFile.createNewFile();
            }
            ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(docxFile));

            Enumeration<? extends ZipEntry> entries = docxTempFile.entries();
            int len = 1;
            byte[] buffer = new byte[1024];
            while (entries.hasMoreElements()) {
                ZipEntry next = entries.nextElement();
                InputStream inputStream = docxTempFile.getInputStream(next);
                zipOut.putNextEntry(new ZipEntry(next.toString()));
                if("word/document.xml".equals(next.toString())) {
                    FileInputStream in = new FileInputStream(tempFile);
                    while ((len = in.read(buffer)) != -1){
                        zipOut.write(buffer, 0, len);
                    }
                    in.close();
                }else {
                    while ((len = inputStream.read(buffer)) != -1){
                        zipOut.write(buffer, 0, len);
                    }
                    inputStream.close();
                }
            }
            zipOut.close();

        }
    }
}
