package com.example.xml2word;

import java.util.ArrayList;
import java.util.List;

/**
 * @author jiyupeng
 */
public final class DataGenerate {
    private DataGenerate(){}

    public static List<Person> getData() {
        List<Person> persons = new ArrayList<>();
        persons.add(new Person("小李", "男", 31));
        persons.add(new Person("小王", "男", 18));
        persons.add(new Person("小红", "女", 21));
        return persons;
    }
}
