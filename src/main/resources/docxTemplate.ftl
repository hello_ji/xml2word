<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<w:document
    xmlns:wpc="http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas"
    xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
    xmlns:o="urn:schemas-microsoft-com:office:office"
    xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships"
    xmlns:m="http://schemas.openxmlformats.org/officeDocument/2006/math"
    xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:wp14="http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing"
    xmlns:wp="http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing"
    xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main"
    xmlns:w14="http://schemas.microsoft.com/office/word/2010/wordml"
    xmlns:w10="urn:schemas-microsoft-com:office:word"
    xmlns:w15="http://schemas.microsoft.com/office/word/2012/wordml"
    xmlns:wpg="http://schemas.microsoft.com/office/word/2010/wordprocessingGroup"
    xmlns:wpi="http://schemas.microsoft.com/office/word/2010/wordprocessingInk"
    xmlns:wne="http://schemas.microsoft.com/office/word/2006/wordml"
    xmlns:wps="http://schemas.microsoft.com/office/word/2010/wordprocessingShape"
    xmlns:wpsCustomData="http://www.wps.cn/officeDocument/2013/wpsCustomData" mc:Ignorable="w14 w15 wp14">
    <w:body>
        <w:p>
            <w:pPr>
                <w:jc w:val="center"/>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                    <w:b/>
                    <w:sz w:val="36"/>
                    <w:szCs w:val="36"/>
                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                </w:rPr>
            </w:pPr>
            <w:r>
                <w:rPr>
                    <w:rFonts w:hint="eastAsia"/>
                    <w:b/>
                    <w:sz w:val="36"/>
                    <w:szCs w:val="36"/>
                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                </w:rPr>
                <w:t>${title}</w:t>
            </w:r>
        </w:p>
        <w:tbl>
            <w:tblPr>
                <w:tblStyle w:val="3"/>
                <w:tblW w:w="8340" w:type="dxa"/>
                <w:tblInd w:w="0" w:type="dxa"/>
                <w:tblBorders>
                    <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                    <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                    <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                    <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                    <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                    <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                </w:tblBorders>
                <w:tblLayout w:type="fixed"/>
                <w:tblCellMar>
                    <w:top w:w="0" w:type="dxa"/>
                    <w:left w:w="108" w:type="dxa"/>
                    <w:bottom w:w="0" w:type="dxa"/>
                    <w:right w:w="108" w:type="dxa"/>
                </w:tblCellMar>
            </w:tblPr>
            <w:tblGrid>
                <w:gridCol w:w="2778"/>
                <w:gridCol w:w="2781"/>
                <w:gridCol w:w="2781"/>
            </w:tblGrid>
            <w:tr>
                <w:tblPrEx>
                    <w:tblBorders>
                        <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                        <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                        <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                        <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                        <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                        <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                    </w:tblBorders>
                    <w:tblLayout w:type="fixed"/>
                    <w:tblCellMar>
                        <w:top w:w="0" w:type="dxa"/>
                        <w:left w:w="108" w:type="dxa"/>
                        <w:bottom w:w="0" w:type="dxa"/>
                        <w:right w:w="108" w:type="dxa"/>
                    </w:tblCellMar>
                </w:tblPrEx>
                <w:trPr>
                    <w:trHeight w:val="492" w:hRule="atLeast"/>
                </w:trPr>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2778" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p>
                        <w:pPr>
                            <w:widowControl w:val="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="default"/>
                                <w:vertAlign w:val="baseline"/>
                                <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:vertAlign w:val="baseline"/>
                                <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                            </w:rPr>
                            <w:t>姓名</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2781" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p>
                        <w:pPr>
                            <w:widowControl w:val="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="default"/>
                                <w:vertAlign w:val="baseline"/>
                                <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:vertAlign w:val="baseline"/>
                                <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                            </w:rPr>
                            <w:t>性别</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
                <w:tc>
                    <w:tcPr>
                        <w:tcW w:w="2781" w:type="dxa"/>
                        <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                        <w:vAlign w:val="center"/>
                    </w:tcPr>
                    <w:p>
                        <w:pPr>
                            <w:widowControl w:val="0"/>
                            <w:jc w:val="center"/>
                            <w:rPr>
                                <w:rFonts w:hint="default"/>
                                <w:vertAlign w:val="baseline"/>
                                <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                            </w:rPr>
                        </w:pPr>
                        <w:r>
                            <w:rPr>
                                <w:rFonts w:hint="eastAsia"/>
                                <w:vertAlign w:val="baseline"/>
                                <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                            </w:rPr>
                            <w:t>年龄</w:t>
                        </w:r>
                    </w:p>
                </w:tc>
            </w:tr>
            <#list persons as person>
                <w:tr>
                    <w:tblPrEx>
                        <w:tblBorders>
                            <w:top w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                            <w:left w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                            <w:bottom w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                            <w:right w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                            <w:insideH w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                            <w:insideV w:val="single" w:color="auto" w:sz="4" w:space="0"/>
                        </w:tblBorders>
                        <w:tblLayout w:type="fixed"/>
                        <w:tblCellMar>
                            <w:top w:w="0" w:type="dxa"/>
                            <w:left w:w="108" w:type="dxa"/>
                            <w:bottom w:w="0" w:type="dxa"/>
                            <w:right w:w="108" w:type="dxa"/>
                        </w:tblCellMar>
                    </w:tblPrEx>
                    <w:trPr>
                        <w:trHeight w:val="520" w:hRule="atLeast"/>
                    </w:trPr>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="2778" w:type="dxa"/>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:widowControl w:val="0"/>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:rFonts w:hint="default"/>
                                    <w:vertAlign w:val="baseline"/>
                                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:hint="eastAsia"/>
                                    <w:vertAlign w:val="baseline"/>
                                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                </w:rPr>
                                <w:t>${(person.name)!}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="2781" w:type="dxa"/>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:widowControl w:val="0"/>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:rFonts w:hint="default"/>
                                    <w:vertAlign w:val="baseline"/>
                                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:hint="eastAsia"/>
                                    <w:vertAlign w:val="baseline"/>
                                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                </w:rPr>
                                <w:t>${(person.gender)!}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                    <w:tc>
                        <w:tcPr>
                            <w:tcW w:w="2781" w:type="dxa"/>
                            <w:shd w:val="clear" w:color="auto" w:fill="auto"/>
                            <w:vAlign w:val="center"/>
                        </w:tcPr>
                        <w:p>
                            <w:pPr>
                                <w:widowControl w:val="0"/>
                                <w:jc w:val="center"/>
                                <w:rPr>
                                    <w:rFonts w:hint="default"/>
                                    <w:vertAlign w:val="baseline"/>
                                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                </w:rPr>
                            </w:pPr>
                            <w:r>
                                <w:rPr>
                                    <w:rFonts w:hint="eastAsia"/>
                                    <w:vertAlign w:val="baseline"/>
                                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                                </w:rPr>
                                <w:t>${(person.age)!}</w:t>
                            </w:r>
                        </w:p>
                    </w:tc>
                </w:tr>
            </#list>
        </w:tbl>
        <w:p>
            <w:pPr>
                <w:jc w:val="left"/>
                <w:rPr>
                    <w:rFonts w:hint="default"/>
                    <w:lang w:val="en-US" w:eastAsia="zh-CN"/>
                </w:rPr>
            </w:pPr>
        </w:p>
        <w:sectPr>
            <w:pgSz w:w="11906" w:h="16838"/>
            <w:pgMar w:top="1440" w:right="1800" w:bottom="1440" w:left="1800" w:header="851" w:footer="992" w:gutter="0"/>
            <w:cols w:space="425" w:num="1"/>
            <w:docGrid w:type="lines" w:linePitch="312" w:charSpace="0"/>
        </w:sectPr>
    </w:body>
</w:document>
